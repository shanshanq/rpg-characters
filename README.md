# RPG Characters
A console application in Java following the given requirements.

## Requirements
- Various character classes: Character(Mage, Ranger, Rogue, Warrior) - `src/com/company/characters`
- Equipment: Item (Weapon, Armor) - `src/com/company/items`
- Custom exceptions: InvalidWeaponException, InvalidArmorException - `src/com/company/exceptions`
- Full test coverage of the functionality: CharacterTest, ItemTest - `tests/`

## Run the tests
Just run the tests in the `tests/` directory.