package com.company.items;

import com.company.enums.WeaponType;

public class Weapon extends Item {

    private int damage;
    private double attackSpeed;
    private WeaponType weaponType;

    public Weapon(){}

    public Weapon(int damage, double attackSpeed, WeaponType weaponType) {
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.weaponType = weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }
}
