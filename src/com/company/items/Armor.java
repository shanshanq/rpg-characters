package com.company.items;

import com.company.PrimaryAttribute;
import com.company.enums.ArmorType;

public class Armor extends Item {
    ArmorType armorType;
    PrimaryAttribute attribute;

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public PrimaryAttribute getAttribute() {
        return attribute;
    }

    public void setAttribute(PrimaryAttribute attribute) {
        this.attribute = attribute;
    }
}
