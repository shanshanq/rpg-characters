package com.company.enums;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
