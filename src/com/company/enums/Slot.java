package com.company.enums;

public enum Slot{
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
