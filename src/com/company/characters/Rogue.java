package com.company.characters;

import com.company.PrimaryAttribute;
import com.company.enums.ArmorType;
import com.company.enums.Slot;
import com.company.enums.WeaponType;
import com.company.items.Item;
import com.company.items.Weapon;

import java.util.ArrayList;
import java.util.List;

public class Rogue extends Character{

    public Rogue(String name) {
        super(name);
        setBase(new PrimaryAttribute(2, 6, 1, 8));
        setWeaponsAllowed(new ArrayList<>(List.of(WeaponType.DAGGER,WeaponType.SWORD)));
        setArmorAllowed(new ArrayList<>(List.of(ArmorType.LEATHER,ArmorType.MAIL)));
    }

    @Override
    public void levelUp() {
        level++;
        base.setStrength(base.getStrength() + 1);
        base.setDexterity(base.getDexterity() + 4);
        base.setIntelligence(base.getIntelligence() + 1);
        base.setVitality(base.getVitality() + 3);
    }

    @Override
    public void calculateDPS() {
        if(equipment.get(Slot.WEAPON) != null){
            // Have weapon equipped
            Item item = equipment.get(Slot.WEAPON);
            if(item instanceof Weapon){
                dps = (((Weapon)item).getDamage() * ((Weapon)item).getAttackSpeed()) * (1 + total.getDexterity()/ 100);
            }
        }else{
            // No weapon equipped
            dps = 1 * (1 + base.getDexterity()/ 100);
        }
    }
}
