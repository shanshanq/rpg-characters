package com.company.characters;

import com.company.PrimaryAttribute;
import com.company.enums.ArmorType;
import com.company.enums.Slot;
import com.company.enums.WeaponType;
import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.items.Armor;
import com.company.items.Item;
import com.company.items.Weapon;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Character {
    protected String name;
    protected int level;
    protected PrimaryAttribute base;
    protected PrimaryAttribute total;
    protected ArrayList<WeaponType> weaponsAllowed;
    protected ArrayList<ArmorType> armorAllowed;
    protected HashMap<Slot, Item> equipment;
    protected double dps;

    public Character(String name){
        this.level = 1;
        this.name = name;
        this.equipment= new HashMap<>();
        this.total = new PrimaryAttribute(0,0,0,0);
    }

    // Getters and setters
    public double getDps() { return dps; }

    public int getLevel() { return level; }

    public void setBase(PrimaryAttribute base) { this.base = base; }

    public void setWeaponsAllowed(ArrayList<WeaponType> weaponsAllowed) {
        this.weaponsAllowed = weaponsAllowed;
    }

    public void setArmorAllowed(ArrayList<ArmorType> armorAllowed) {
        this.armorAllowed = armorAllowed;
    }

    // Equip the given weapon, return true if the weapon is valid
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weaponsAllowed.contains(weapon.getWeaponType()) && level >= weapon.getRequiredLevel()){
            equipment.put(Slot.WEAPON, weapon);
            return true;
        }else throw new InvalidWeaponException("The weapon you want to equip is invalid!");
    }

    // Equip the given armor, return true if the armor is valid
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        if (armorAllowed.contains(armor.getArmorType()) && level >= armor.getRequiredLevel()){
            equipment.put(armor.getSlot(),armor);
            updateTotalAttributes(armor.getAttribute());
            return true;
        }else throw new InvalidArmorException("The armor you want to equip is invalid!");
    }

    // Update the total attributes after armor equipped
    protected void updateTotalAttributes(PrimaryAttribute attribute){
        total.setStrength(base.getStrength() + attribute.getStrength());
        total.setDexterity(base.getDexterity() + attribute.getDexterity());
        total.setIntelligence(base.getIntelligence() + attribute.getIntelligence());
        total.setVitality(base.getVitality() + attribute.getVitality());
    };

    // Abstract methods
    public abstract void levelUp();
    public abstract void calculateDPS();

    // Display the stats
    public String displayStats() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: " + name);
        sb.append("\n");
        sb.append("Level: " + level + " ");
        sb.append("\n");
        sb.append("Total attribute stats: "+ total.toString());
        sb.append("\n");
        sb.append("DPS: "+ dps );
        return sb.toString();
    }
}