package com.company.characters;

import com.company.PrimaryAttribute;
import com.company.enums.ArmorType;
import com.company.enums.Slot;
import com.company.enums.WeaponType;
import com.company.items.Item;
import com.company.items.Weapon;

import java.util.ArrayList;
import java.util.List;

public class Warrior extends Character {
    public Warrior(String name) {
        super(name);
        setBase(new PrimaryAttribute(5, 2, 1, 10));
        setWeaponsAllowed(new ArrayList<>(List.of(WeaponType.AXE,WeaponType.HAMMER,WeaponType.SWORD)));
        setArmorAllowed(new ArrayList<>(List.of(ArmorType.MAIL,ArmorType.PLATE)));
    }

    @Override
    public void levelUp() {
        level++;
        base.setStrength(base.getStrength() + 3);
        base.setDexterity(base.getDexterity() + 2);
        base.setIntelligence(base.getIntelligence() + 1);
        base.setVitality(base.getVitality() + 5);
    }

    @Override
    public void calculateDPS() {
        if(equipment.get(Slot.WEAPON) != null){
            // Have weapon equipped
            Item item = equipment.get(Slot.WEAPON);
            if(item instanceof Weapon){
                dps = (((Weapon)item).getDamage() * ((Weapon)item).getAttackSpeed()) * (1 + total.getStrength()/ 100);
            }
        }else{
            // No weapon equipped
            dps = 1 * (1 + base.getStrength()/ 100);
        }
    }
}
