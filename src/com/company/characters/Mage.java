package com.company.characters;

import com.company.PrimaryAttribute;
import com.company.enums.ArmorType;
import com.company.enums.Slot;
import com.company.enums.WeaponType;
import com.company.items.Item;
import com.company.items.Weapon;

import java.util.ArrayList;
import java.util.List;


public class Mage extends Character {

    public Mage(String name) {
        super(name);
        setBase(new PrimaryAttribute(1, 1, 8, 5));
        setWeaponsAllowed(new ArrayList<>(List.of(WeaponType.STAFF, WeaponType.WAND)));
        setArmorAllowed(new ArrayList<>(List.of(ArmorType.CLOTH)));
    }

    @Override
    public void levelUp() {
        level++;
        base.setStrength(base.getStrength() + 1);
        base.setDexterity(base.getDexterity() + 1);
        base.setIntelligence(base.getIntelligence() + 5);
        base.setVitality(base.getVitality() + 3);
    }

    @Override
    public String toString() {
        return "Mage{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", primaryAttribute=" + base.toString() +
                '}';
    }

    @Override
    public void calculateDPS() {
        if(equipment.get(Slot.WEAPON) != null){
            // Have weapon equipped
            Item item = equipment.get(Slot.WEAPON);
            if(item instanceof Weapon){
                dps = (((Weapon)item).getDamage() * ((Weapon)item).getAttackSpeed()) * (1 + total.getIntelligence()/ 100);
            }
        }else{
            // No weapon equipped
            dps = 1 * (1 + base.getIntelligence()/ 100);
        }
    }

}
