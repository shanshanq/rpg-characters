package com.company.items;

import com.company.PrimaryAttribute;
import com.company.characters.Mage;
import com.company.characters.Warrior;
import com.company.enums.ArmorType;
import com.company.enums.Slot;
import com.company.enums.WeaponType;
import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    // Items and equipment tests
    @Test
    void character_equipHighLevelWeapon_ShouldThrowInvalidWeaponException() {
        // Arrange
        Warrior warrior= new Warrior("Warrior") ;
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setRequiredLevel(2);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setWeaponType(WeaponType.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        // Act
        // Assert
        assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipWeapon(testWeapon);
        });
    }

    @Test
    void character_equipHighLevelArmor_ShouldThrowInvalidArmorException() {
        // Arrange
        Warrior warrior= new Warrior("Warrior") ;
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setRequiredLevel(2);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setAttribute(new PrimaryAttribute(1,0,0,2));
        // Act
        // Assert
        assertThrows(InvalidArmorException.class, () -> {
            warrior.equipArmor(testPlateBody);
        });
    }

    @Test
    void character_equipWrongTypeWeapon_ShouldThrowInvalidWeaponException() {
        // Arrange
        Warrior warrior= new Warrior("Warrior") ;
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Bow");
        testWeapon.setRequiredLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setWeaponType(WeaponType.BOW);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        // Act
        // Assert
        assertThrows(InvalidWeaponException.class, () -> {
            warrior.equipWeapon(testWeapon);
        });
    }

    @Test
    void character_equipWrongTypeArmor_ShouldThrowInvalidArmorException() {
        // Arrange
        Warrior warrior= new Warrior("Warrior") ;
        Armor testClothBody = new Armor();
        testClothBody.setName("Common Cloth Body Armor");
        testClothBody.setRequiredLevel(1);
        testClothBody.setSlot(Slot.BODY);
        testClothBody.setArmorType(ArmorType.CLOTH);
        testClothBody.setAttribute(new PrimaryAttribute(1,0,0,2));
        // Act
        // Assert
        assertThrows(InvalidArmorException.class, () -> {
            warrior.equipArmor(testClothBody);
        });
    }

    @Test
    void character_equipValidWeapon_ShouldReturnTrue() throws InvalidWeaponException {
        // Arrange
        Warrior warrior= new Warrior("Warrior") ;
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setRequiredLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setWeaponType(WeaponType.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        boolean actual;
        // Act
        actual = warrior.equipWeapon(testWeapon);
        // Assert
        assertTrue(actual);
    }

    @Test
    void character_equipValidArmor_ShouldReturnTrue() throws InvalidArmorException {
        // Arrange
        Warrior warrior= new Warrior("Warrior") ;
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setRequiredLevel(1);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setAttribute(new PrimaryAttribute(1,0,0,2));
        boolean actual;
        // Act
        actual = warrior.equipArmor(testPlateBody);
        // Assert
        assertTrue(actual);
    }

    @Test
    void calculateDPS_noWeaponEquipped_ShouldBeASExpected(){
        // Arrange
        Warrior warrior= new Warrior("Warrior") ;
        double expected = 1 * (1 + (5 / 100));
        // Act
        warrior.calculateDPS();
        double actual = warrior.getDps();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void calculateDPS_ValidWeaponEquipped_ShouldBeASExpected() throws InvalidWeaponException {
        // Arrange
        Warrior warrior= new Warrior("Warrior") ;
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setRequiredLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setWeaponType(WeaponType.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        warrior.equipWeapon(testWeapon);
        double expected = ( 7 * 1.1) * (1 + (5 / 100));
        // Act
        warrior.calculateDPS();
        double actual = warrior.getDps();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void calculateDPS_ValidWeaponArmorEquipped_ShouldBeASExpected() throws InvalidArmorException, InvalidWeaponException {
        // Arrange
        Warrior warrior= new Warrior("Warrior") ;
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setRequiredLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setWeaponType(WeaponType.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        warrior.equipWeapon(testWeapon);

        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setRequiredLevel(1);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setAttribute(new PrimaryAttribute(1,0,0,2));
        warrior.equipArmor(testPlateBody);

        double expected = ( 7 * 1.1) * (1 + ((5+1) / 100));
        // Act
        warrior.calculateDPS();
        double actual = warrior.getDps();
        // Assert
        assertEquals(expected,actual);
    }

}