package com.company.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {
    // Character attribute and level tests
    @Test
    void character_created_ShouldBeLevel1() {
        // Arrange
        int expected = 1;
        // Act
        Mage mage = new Mage("test") ;
        int actual = mage.getLevel();
        // Assert
        assertEquals(expected,actual);
    }
    @Test
    void levelUp_gainALevel_ShouldBeLevel2(){
        // Arrange
        int expected = 2;
        // Act
        Ranger ranger = new Ranger("test") ;
        ranger.levelUp();
        int actual = ranger.getLevel();
        // Assert
        assertEquals(expected,actual);
    }
    @Test
    void mage_created_ShouldHaveDefaultAttributes(){
        // Arrange
        int expected_s = 1 ;
        int expected_d = 1 ;
        int expected_i = 8 ;
        int expected_v = 5 ;
        // Act
        Mage mage = new Mage("test") ;
        int actual_s = mage.base.getStrength();
        int actual_d = mage.base.getDexterity();
        int actual_i = mage.base.getIntelligence();
        int actual_v = mage.base.getVitality();
        // Assert
        assertEquals(expected_s,actual_s);
        assertEquals(expected_d,actual_d);
        assertEquals(expected_i,actual_i);
        assertEquals(expected_v,actual_v);
    }
    @Test
    void ranger_created_ShouldHaveDefaultAttributes(){
        // Arrange
        int expected_s = 1 ;
        int expected_d = 7 ;
        int expected_i = 1 ;
        int expected_v = 8 ;
        // Act
        Ranger ranger = new Ranger("test") ;
        int actual_s = ranger.base.getStrength();
        int actual_d = ranger.base.getDexterity();
        int actual_i = ranger.base.getIntelligence();
        int actual_v = ranger.base.getVitality();
        // Assert
        assertEquals(expected_s,actual_s);
        assertEquals(expected_d,actual_d);
        assertEquals(expected_i,actual_i);
        assertEquals(expected_v,actual_v);
    }
    @Test
    void rogue_created_ShouldHaveDefaultAttributes(){
        // Arrange
        int expected_s = 2 ;
        int expected_d = 6 ;
        int expected_i = 1 ;
        int expected_v = 8 ;
        // Act
        Rogue rogue = new Rogue("test") ;
        int actual_s = rogue.base.getStrength();
        int actual_d = rogue.base.getDexterity();
        int actual_i = rogue.base.getIntelligence();
        int actual_v = rogue.base.getVitality();
        // Assert
        assertEquals(expected_s,actual_s);
        assertEquals(expected_d,actual_d);
        assertEquals(expected_i,actual_i);
        assertEquals(expected_v,actual_v);
    }
    @Test
    void warrior_created_ShouldHaveDefaultAttributes(){
        // Arrange
        int expected_s = 5 ;
        int expected_d = 2 ;
        int expected_i = 1 ;
        int expected_v = 10 ;
        // Act
        Warrior warrior= new Warrior("test") ;
        int actual_s = warrior.base.getStrength();
        int actual_d = warrior.base.getDexterity();
        int actual_i = warrior.base.getIntelligence();
        int actual_v = warrior.base.getVitality();
        // Assert
        assertEquals(expected_s,actual_s);
        assertEquals(expected_d,actual_d);
        assertEquals(expected_i,actual_i);
        assertEquals(expected_v,actual_v);
    }
    @Test
    void mage_levelUp_ShouldHaveAttributesIncreased(){
        // Arrange
        int expected_s = 2 ;
        int expected_d = 2 ;
        int expected_i = 13 ;
        int expected_v = 8 ;
        Mage mage = new Mage("test") ;
        // Act
        mage.levelUp();
        int actual_s = mage.base.getStrength();
        int actual_d = mage.base.getDexterity();
        int actual_i = mage.base.getIntelligence();
        int actual_v = mage.base.getVitality();
        // Assert
        assertEquals(expected_s,actual_s);
        assertEquals(expected_d,actual_d);
        assertEquals(expected_i,actual_i);
        assertEquals(expected_v,actual_v);
    }
    @Test
    void ranger_levelUp_ShouldHaveAttributesIncreased(){
        // Arrange
        int expected_s = 2 ;
        int expected_d = 12 ;
        int expected_i = 2 ;
        int expected_v = 10 ;
        Ranger ranger = new Ranger("test") ;
        // Act
        ranger.levelUp();
        int actual_s = ranger.base.getStrength();
        int actual_d = ranger.base.getDexterity();
        int actual_i = ranger.base.getIntelligence();
        int actual_v = ranger.base.getVitality();
        // Assert
        assertEquals(expected_s,actual_s);
        assertEquals(expected_d,actual_d);
        assertEquals(expected_i,actual_i);
        assertEquals(expected_v,actual_v);
    }
    @Test
    void rogue_levelUp_ShouldHaveAttributesIncreased(){
        // Arrange
        int expected_s = 3 ;
        int expected_d = 10 ;
        int expected_i = 2 ;
        int expected_v = 11 ;
        Rogue rogue = new Rogue("test") ;
        // Act
        rogue.levelUp();
        int actual_s = rogue.base.getStrength();
        int actual_d = rogue.base.getDexterity();
        int actual_i = rogue.base.getIntelligence();
        int actual_v = rogue.base.getVitality();
        // Assert
        assertEquals(expected_s,actual_s);
        assertEquals(expected_d,actual_d);
        assertEquals(expected_i,actual_i);
        assertEquals(expected_v,actual_v);
    }

    @Test
    void warrior_levelUp_ShouldHaveAttributesIncreased(){
        // Arrange
        int expected_s = 8 ;
        int expected_d = 4 ;
        int expected_i = 2 ;
        int expected_v = 15 ;
        Warrior warrior= new Warrior("test") ;
        // Act
        warrior.levelUp();
        int actual_s = warrior.base.getStrength();
        int actual_d = warrior.base.getDexterity();
        int actual_i = warrior.base.getIntelligence();
        int actual_v = warrior.base.getVitality();
        // Assert
        assertEquals(expected_s,actual_s);
        assertEquals(expected_d,actual_d);
        assertEquals(expected_i,actual_i);
        assertEquals(expected_v,actual_v);
    }
}